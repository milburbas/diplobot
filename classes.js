const Game = class {
	constructor(name) {
		this.name = name
		this.players = []
	}

	addPlayer(name) {
		const player = {
			name: name,
		}
		this.players.push(player)
	}
}