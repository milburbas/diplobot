global.data = require('./global.json')
auth = require('./auth.json');

const log_prefix = "\x1b[35mMain >> \x1b[0m%s"
const loglevel = 3

function log(message, level = 0) {
	if (level > loglevel) return
	console.log(log_prefix,message)
}

const fs = require('fs');

global.bot = this

const { Client, Collection, Intents, MessageAttachment } = require("discord.js")
global.client = new Client({
	intents: [
	Intents.FLAGS.GUILDS,
	Intents.FLAGS.GUILD_MEMBERS,
    Intents.FLAGS.GUILD_MESSAGES,
	Intents.FLAGS.GUILD_MESSAGE_REACTIONS
],
	partials: [
		'MESSAGE', 'CHANNEL', 'REACTION', 'GUILD_MEMBER', 'USER'
]
});

global.client.login(auth.token);
global.client.on('ready', () => {
	global.commands = require("./modules/commands/commands.js")
	global.commands.initFn.init()
	global.games = require("./modules/games/games.js")
	global.games.initFn.init()
	global.karma = require("./modules/karma/karma.js")
	global.karma.initFn.init()
	global.say = require("./modules/say/say.js")
	global.say.initFn.init()
	global.flagdesigner = require("./modules/flagdes/flagdes.js")
	global.flagdesigner.initFn.init()
	log("Online")
})

global.client.on('error', err => {
	log(err)
})

setInterval(save, 5000)

function save() {
	fs.readFile("./global.json", 'utf8', function(err, data){
		if (data != JSON.stringify(global.data)) {
			log("saving database")
			fs.writeFileSync("./global.json", JSON.stringify(global.data))
			async function backup() {
			 const channel = await global.client.channels.cache.get("882275821610491995")
			 channel.send({ files: [new MessageAttachment("./global.json")] })
			}
			backup()
		}
	});
}

global.client.on('disconnect', () => {
	log("Offline, attempting reconnect")
	global.client.login(auth.token);
})

global.client.on('messageCreate', (message) => {
	if (message.author.id != "370281623394189322") return
	switch(message.content.replace("*restart ", '')) {
		case "commands":
			log("restarting commands module!")
			global.commands.initFn.uninit()
			global.commands = require("./modules/commands/commands.js")
			global.commands.initFn.init()
			break
		case "games":
			log("restarting games module!")
			global.games.initFn.uninit()
			global.games = require("./modules/games/games.js")
			global.games.initFn.init()
			break
	}
})

global.client.on("messageCreate", (message) => {
	if (message.content.toLowerCase().includes("does wilder stink")) {
		message.reply(`Yes.`)
	}
	// if (message.author.id == "515593074282332183") {
	// 	async function stinky() {
	// 		await message.react("🇸")
	// 		await message.react("🇹")
	// 		await message.react("🇮")
	// 		await message.react("🇳")
	// 		await message.react("🇰")
	// 		await message.react("🇾")
	// 	}
	// 	if (Math.random() * 100 < 3) stinky()
	// }
})
