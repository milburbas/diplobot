const loglevel = 5
const log_prefix = "\x1b[36mGame >> \x1b[0m%s"

const Game = class {
	constructor(name, id) {
		this.state = "pending"
		this.id = id
		this.name = name
		this.data = {
			"turn": 0,
			"reminder": 3600 * 24,
			"begin": 0,
			"history": []
		}
		this.channels = {
			"category": "",
			//"info": "",
			"chat": "",
			"policies": "",
			"wars-pacts": "",
			"roleplay": "",
			"news": ""
		}
		this.role = ""
		this.players = {}
	}
	setWatchMessage(message_id) {
		this.watchMessage = message_id
	}
}
gamePoll = {
	title: "Map Setup",
	description: "What should the map be?",
	showvoters: true,
	restricted: true,
	fields: [
		{
			title: "Terrain",
			id: "terrain",
			options: [
				{
					label: 'Dryland',
					description: 'No water.',
					value: 'dryland',
				},
				{
					label: 'Lakes',
					description: 'Some Water.',
					value: 'lakes',
				},
				{
					label: 'Continents',
					description: 'More Water.',
					value: 'continents',
				},
				{
					label: 'Archipelago',
					description: 'More Water than Land.',
					value: 'archipelago',
				},
				{
					label: 'Waterworld',
					description: 'So you\'re that type of person, huh...',
					value: 'waterworld',
				}
			]
		},
		{
			title: "Size",
			id: "size",
			options: [
				{
					label: 'Tiny',
					description: '121 Tiles.',
					value: 'tiny',
				},
				{
					label: 'Small',
					description: '196 Tiles.',
					value: 'small',
				},
				{
					label: 'Normal',
					description: '256 Tiles.',
					value: 'normal',
				},
				{
					label: 'Large',
					description: '324 Tiles.',
					value: 'large',
				},
				{
					label: 'Huge',
					description: '400 Tiles.',
					value: 'huge',
				},
				{
					label: 'Massive',
					description: '900 Tiles.',
					value: 'massive',
				},
			]
		},
		{
			title: "Anti-Snowball Rules?",
			id: "snowballrules",
			options: [
				{
					label: 'Yes',
					description: 'Cities cannot be captured until a peace treaty is made.',
					value: 'yes',
				},
				{
					label: 'No',
					description: 'No, cities can be captured ASAP.',
					value: 'no',
				}
			]
		}
	]
}

var running = false

function log(message, level = 0) {
	if (level > loglevel) return
	console.log(log_prefix, message)
}

exports.initFn = {
	init,
	uninit
}

exports.game = {
	newGame,
	startGame,
	joinGame,
	watchGame,
	setPolicy,
	say,
	info,
	sayToggle,
	checkMention,
	commandPoll
}

function init() {
	running = true
	log("Initialized")
}

function uninit() {
	delete require.cache[require.resolve("./games.js")];
	running = false
	log("Uninitialized")
}

const { MessageActionRow, MessageEmbed, MessageButton, MessageAttachment, MessageSelectMenu, Webhook } = require('discord.js');

function commandPoll(interaction) {
	let game = findGameByChannel(interaction.guild.id, interaction.channel.id)
	if (!game) {
		interaction.reply({ ephemeral: true, content: "You are not in a game channel." })
		return
	}
	let cPoll = {
		title: "Poll",
		showvoters: interaction.options.get("showvoters"),
		restricted: interaction.options.get("restricted"),
		fields: [
				{
 			title: interaction.options.getString("title"),
			id: "pollbody",
 			options: [
 				{
 					label: interaction.options.getString("optone"),
 					description: interaction.options.getString("descriptione"),
 					value: 'opt1',
 				},
 				{
 					label: interaction.options.getString("optwo"),
 					description: interaction.options.getString("descriptwo"),
 					value: 'opt2',
 				},
 			]
 		}
		]
	}
	poll(game, cPoll, interaction.channel)
}


// poll = {
// 	title: "Poll Title",
// 	description: "This is a description of the poll",
//  showvoters: true
// 	restricted: true, //Players only?
// 	fields: [
// 		{
// 			title: "Section Title",
//			id: "secid"
// 			options: [
// 				{
// 					label: 'Option 1',
// 					description: 'This is option 1.',
// 					value: 'opt1',
// 				},
// 				{
// 					label: 'Option 2',
// 					description: 'This is option 2.',
// 					value: 'opt2',
// 				},
// 			]
// 		}
// 	]
// }

async function poll(game, poll, channel) {
	const embed = new MessageEmbed()
		.setColor('#0099ff')
		.setTitle(poll.title)
		.setDescription("This is a poll. Anyone may vote in it. Select as many options for each question as you support.")
	if (poll.restricted) embed.setDescription("This is a poll. Only players of this game may vote in it. Select as many options for each question as you support.")
	const rows = []
	fieldIds = []
	pollData = { "poll": poll, "results": { ".voters": [] } }
	poll.fields.forEach(field => {
		row = new MessageActionRow()
		row.addComponents(
			new MessageSelectMenu()
				.setCustomId(field.id)
				.setPlaceholder(field.title)
				.setMinValues(1)
				.setMaxValues(field.options.length)
				.addOptions(field.options),
		);
		rows.push(row)
		let results = ""
		pollData.results[field.id] = {}
		field.options.forEach(option => {
			results += `\n${option.label}: 0%`
			pollData.results[field.id][option.value] = []
		})
		embed.addFields({ name: `**${field.title}**`, value: results })
	})
	channel.send({ embeds: [embed], components: rows }).then(message => {
		if (!Object.keys(game).includes("polls")) game["polls"] = {}
		game.polls[message.id] = pollData
	})
	//game.channels.chat.send({embeds: embed, components: [row]})
}

client.on("interactionCreate", async interaction => {
	if (!interaction.isSelectMenu()) return
	let game = findGameByChannel(interaction.guild.id, interaction.channel.id)
	if (!game) return
	if (!Object.keys(game).includes("polls")) return
	if (!Object.keys(game.polls).includes(interaction.message.id)) return
	let poll = game.polls[interaction.message.id]
	if (poll.poll.restricted) {
		if (!Object.keys(game.players).includes(interaction.user.id)) {
			interaction.reply({ ephemeral: true, content: "You can't vote in this poll!" })
			return
		}
	}
	const embed = new MessageEmbed()
		.setColor('#0099ff')
		.setTitle(poll.poll.title)
		.setDescription("This is a poll. Anyone may vote in it. Select as many options for each question as you support.")
	if (poll.poll.restricted) embed.setDescription("This is a poll. Only players of this game may vote in it. Select as many options for each question as you support.")
	const rows = []
	if (!poll.results[".voters"].includes(interaction.user.id)) poll.results[".voters"].push(interaction.user.id)
	Object.keys(poll.results).forEach(result => {
		if (!result.startsWith(".") && result == interaction.customId) {
			Object.keys(poll.results[result]).forEach(option => {
				let optionResults = poll.results[result][option]
				if (interaction.values.includes(option)) {
					if (!optionResults.includes(interaction.user.id)) {
						optionResults.push(interaction.user.id)
					}
				} else {
					if (optionResults.includes(interaction.user.id)) {
						optionResults.splice(optionResults.indexOf(interaction.user.id), 1)
					}
				}
			})
		}
	})
	await asyncForEach(poll.poll.fields, async (field) => {
		row = new MessageActionRow()
		row.addComponents(
			new MessageSelectMenu()
				.setCustomId(field.id)
				.setPlaceholder(field.title)
				.setMinValues(1)
				.setMaxValues(field.options.length)
				.addOptions(field.options),
		);
		rows.push(row)
		let results = ""
		await asyncForEach(field.options, async option => {
			let optionResults = poll.results[field.id][option.value]
			results += `\n${option.label}: ${Math.floor((optionResults.length / poll.results[".voters"].length) * 100)}% `
			await asyncForEach(optionResults, async supporter => {
				user = await client.users.fetch(supporter)
				if (poll.poll.showvoters) {
					emoji = await getEmoji(user)
					results += emoji
				}
			})
		})
		embed.addFields({ name: `**${field.title}**`, value: results })
	})
	interaction.update({ embeds: [embed], components: rows })
})

client.on("interactionCreate", interaction => {
	if (!interaction.isCommand()) return
	switch (interaction.commandName) {
		case "me":
			interaction.reply({ ephemeral: true, content: "Tank." })
			if (!Object.keys(global.data).includes("users")) {
				global.data["users"] = {}
			}
			global.data.users[interaction.user.id] = { username: interaction.options.getString("username") }
			break
		case "getinvites":
			let game = findGameByChannel(interaction.guild.id, interaction.channel.id)
			if (!game) {
				interaction.reply({ ephemeral: true, content: "You are not in a game channel." })
				return
			}
			const embed = new MessageEmbed()
				.setColor('#0099ff')
				.setTitle("Invites")
				.setDescription("")
			Object.values(game.players).forEach(player => {
				if (Object.keys(data.users).includes(player.id)) {
					embed.description += `\n<@!${player.id}>: ${data.users[player.id].username}`
				} else {
					embed.description += `\n<@!${player.id}> cannot be found.`
				}
			})
			interaction.reply({ ephemeral: true, embeds: [embed] })
			break

	}
})

async function newGame(guild_id) {
	log("creating new game in " + String(guild_id))
	const games = global.data[guild_id].games
	log("finding ID", 3)
	var id = 0
	games.forEach(game => {
		if (game.id >= id) id = game.id + 1
		log(id, 3)
	});
	var game = new Game("Game " + String(id), id)
	games.push(game)
	log(game, 2)
	global.commands.commands.updateCommand("info", guild_id)
	global.commands.commands.updateCommand("start", guild_id)
	const row = new MessageActionRow()
		.addComponents(
			new MessageButton()
				.setCustomId("{" + String(id) + "}" + "[join]")
				.setLabel('Join/Leave Game')
				.setStyle('PRIMARY'),
		);

	const embed = new MessageEmbed()
		.setColor('#0099ff')
		.setTitle('New Game Pending')
		.setDescription(game.name + " is now pending!")
		.addFields({ name: `**Players: ${Object.keys(game.players).length}**`, value: "No Current Players" })
	const channel = await global.client.channels.cache.get(global.data[guild_id].announcements)
	channel.send({ embeds: [embed], components: [row] }).then(message => {
		game.setWatchMessage(message.id)
	})
}

async function startGame(guild_id, game_id) {
	log("starting game " + String(game_id))
	var game = global.data[guild_id].games[parseInt(game_id)]
	game.state = "starting"
	const guild = await global.client.guilds.cache.get(guild_id)
	await guild.roles.create({
		data: { name: game.name }
	}).then(role => {
		Object.values(game.players).forEach(player => {
			try {
				guild.members.fetch(player.id).then(member => {
					member.roles.add(role)
				})
			} catch (error) {
				log(error)
			}
		})
		game.role = role.id
		role.setName(game.name)
		role.setMentionable(true)
	})
	await guild.channels.create(game.name, {
		type: 4,
		permissionOverwrites: [
			{
				id: guild.id,
				deny: ['VIEW_CHANNEL']
			},
			{
				id: game.role,
				allow: ['VIEW_CHANNEL']
			}
		]
	}).then(category => game.channels.category = category.id)
	await asyncForEach(Object.keys(game.channels), async (channel) => {
		if (channel != "category") {
			let newChannel = await guild.channels.create(channel, { parent: game.channels.category })
			if (channel == "chat") {
				poll(game, gamePoll, newChannel)
			}
			game.channels[channel] = newChannel.id
		}
	})
	const watchMessage = await global.client.channels.cache.get(global.data[guild.id].announcements).messages.fetch(game.watchMessage)
	var playerList = ""
	Object.values(game.players).forEach(player => {
		playerList += "\n"
		playerList += "<@!" + String(player.id) + ">"
	})
	if (playerList == "") playerList = "No Current Players"
	const embed = new MessageEmbed()
		.setColor('#0099ff')
		.setTitle(game.name)
		.addFields({ name: `**Players: ${Object.keys(game.players).length}**`, value: playerList })
	const row = new MessageActionRow()
		.addComponents(
			new MessageButton()
				.setCustomId("{" + String(game.id) + "}" + "[watch]")
				.setLabel('Watch Game')
				.setStyle('DANGER'),
		);
	watchMessage.edit(({ embeds: [embed], components: [row] }))
	//const infoChannel = global.client.channels.cache.get(game.channels.info)
	//infoChannel.setRateLimitPerUser(3600)
	const infoEmbed = new MessageEmbed()
		.setColor('#0099ff')
		.setTitle(game.name)
		.addFields({ name: `**Players: ${Object.keys(game.players).length}**`, value: playerList })
	const infoRow = new MessageActionRow()
		.addComponents(
			new MessageButton()
				.setCustomId("{" + String(game.id) + "}" + "[pass]")
				.setLabel('Pass')
				.setStyle('PRIMARY'),
		);
	//infoChannel.send({ embeds: [infoEmbed], components: [infoRow]})
}

function watchGame(guild_id, game_id, user, interaction) {
	log("adding/removing " + String(user.id) + " from game " + String(game_id) + " watchlist", 2)
	var game = global.data[guild_id].games[game_id]
	const channel = global.client.channels.cache.get(game.channels.category)
	var hasPerm = false
	var overwrites = channel.permissionOverwrites.cache.get(user.id)
	if (overwrites != undefined) {
		if (parseInt(overwrites.allow.bitfield) != 0) hasPerm = true
	}
	if (hasPerm) {
		log("removing!", 2)
		interaction.reply({ content: 'Game is now invisible.', ephemeral: true });
		channel.permissionOverwrites.set([{ "id": user }])
	} else {
		log("adding!", 2)
		interaction.reply({ content: 'Game is now visible.', ephemeral: true });
		channel.permissionOverwrites.set([{ "id": user, "allow": "VIEW_CHANNEL" }])
	}
}
function info(guildId, user, interaction) {
	var game = findGameByChannel(guildId, interaction.channel.id)
	if (!game) {
		interaction.reply({ content: 'Not in a game channel!', ephemeral: true });
		return
	}
	interaction.reply({ content: `${Object.keys(game.players)}`, ephemeral: true })
}

function joinGame(guild_id, game_id, user, interaction) {
	var game = global.data[guild_id].games[game_id]
	log("joining/leaving " + String(user.id) + " to/from " + String(game_id), 3)
	var inGame = Object.keys(game.players).includes(user.id)
	if (inGame) {
		log(String(user.id) + " already in game, leaving", 2)
		interaction.reply({ content: 'Left Game.', ephemeral: true });
		delete game.players[user.id]
	} else {
		log("joining " + String(user.id) + " to " + String(game_id), 2)
		interaction.reply({ content: 'Joined Game.', ephemeral: true });
		var player = {
			"id": user.id,
			"turn": -1,
			"bio": {}
		}
		game.players[player.id] = player
	}
	log("updating playerList of " + String(game_id), 3)
	var playerList = ""
	Object.values(game.players).forEach(player => {
		playerList += "\n"
		playerList += "<@!" + String(player.id) + ">"
		log("found " + String(player.id), 4)
	})
	if (playerList == "") playerList = "No Current Players"
	log(playerList, 3)

	interaction.guild.channels.fetch(global.data[guild_id].announcements).then( announceChannel => {
		announceChannel.messages.fetch(game.watchMessage).then( watchMessage => {
			const embed = watchMessage.embeds[0]
			embed.fields[0].name = `**Players: ${Object.keys(game.players).length}**`
			embed.fields[0].value = playerList
			watchMessage.edit({ embeds: [embed] })
		})
	})
}

client.on("guildMemberUpdate", (oldMem, newMem) => {
	try {
		let roles = {}
		let games = global.data[newMem.guild.id].games
		Object.keys(games).forEach(gameId => {
			roles[gameId] = games[gameId].role
		})
		Object.keys(roles).forEach(role => {
			log(`Checking ${roles[role]} for ${newMem.displayName}`, 4)
			if (newMem.roles.cache.has(roles[role])) {
				log(`Updating ${roles[role]} for ${newMem.displayName}`, 2)
				game = games[role]
				var player = {
					"id": newMem.user.id,
					"turn": -1,
					"bio": {}
				}
				if (!Object.keys(game.players).includes(player.id)) {
					game.players[player.id] = player
				}
			}
		})
	} catch {
		log("problem with role update, probably fine", 4)
	}
})

function setPolicy(guild_id, user, interaction) {
	var game = findGameByChannel(guild_id, interaction.channel.id)
	log(game)
	if (interaction.channel.id != game.channels.policies) {
		log(String(user.id) + "    policy command not called in policies channel")
		return
	}
	if (!Object.keys(game.players).includes(user.id)) {
		log(String(user.id) + "    policy command called by non-player")
		return
	}
	let options = [
		{
			label: 'Name',
			description: 'Formal name of your nation.',
			value: 'name',
		},
		{
			label: 'Tribe',
			description: 'Your nation\'s tribe.',
			value: 'tribe',
		},
		{
			label: 'Shorthand',
			description: 'Shortened name of your nation.',
			value: 'shorthand',
		},
		{
			label: 'Ideology',
			description: 'What your nation believes.',
			value: 'ideology',
		},
		{
			label: 'History',
			description: 'How your nation got to this point.',
			value: 'history',
		},
		{
			label: 'Border Policy',
			description: 'How your nation treats its borders.',
			value: 'borders',
		},
		{
			label: 'Values',
			description: 'What your nation will always (or never) do.',
			value: 'values',
		},
		{
			label: 'Religion',
			description: 'What system of beliefs your nation follows.',
			value: 'religion',
		},
		{
			label: 'Culture',
			description: 'What makes your nation\'s society unique.',
			value: 'culture',
		},
		{
			label: 'Flag',
			description: 'The flag your nation flies.',
			value: 'flag',
		},
		{
			label: 'Emblem',
			description: 'The emblem your nation displays.',
			value: 'emblem',
		},
		{
			label: 'Color',
			description: 'The color that best accents your nation.',
			value: 'color',
		},
	]
	const mainRow = new MessageActionRow()
		.addComponents(
			new MessageSelectMenu()
				.setCustomId('policy')
				.setPlaceholder('Nothing selected')
				.setMinValues(1)
				.setMaxValues(options.length)
				.addOptions(options),
		);
	interaction.reply({ content: 'Select what part of your policy to add/modify.', components: [mainRow], ephemeral: true });
}

async function setPolicyCont(guild_id, game_id, sections, interaction) {
	var game = global.data[guild_id].games[game_id]
	var policy = game.players[interaction.user.id].bio
	await interaction.reply({ content: "Loading!", ephemeral: true })
	var finishMessage = "Done!"
	await asyncForEach(sections, async (section) => {
		log(section)
		switch (section) {
			case "name":
				interaction.editReply({ content: "What is your nation called? (Name) \nSay 'Skip' if you'd like to come back to this later, or 'Delete' to remove this section of your policy.", ephemeral: true })
				break
			case "tribe":
				interaction.editReply({ content: "What tribe does your nation belong to? This should be something to the tune of 'Red Oumaji'. (Tribe) \nSay 'Skip' if you'd like to come back to this later, or 'Delete' to remove this section of your policy.", ephemeral: true })
				break
			case "ideology":
				interaction.editReply({ content: "What does your nation believe? (Ideology) \nSay 'Skip' if you'd like to come back to this later, or 'Delete' to remove this section of your policy. Start your message with a + to directly append.", ephemeral: true })
				break
			case "history":
				interaction.editReply({ content: "What has led your nation here? (History) \nSay 'Skip' if you'd like to come back to this later, or 'Delete' to remove this section of your policy. Start your message with a + to directly append.", ephemeral: true })
				break
			case "culture":
				interaction.editReply({ content: "What makes your nation's society unique? (Culture) \nSay 'Skip' if you'd like to come back to this later, or 'Delete' to remove this section of your policy. Start your message with a + to directly append.", ephemeral: true })
				break
			case "religion":
				interaction.editReply({ content: "What system of beliefs does your nation follow? (Religion) \nSay 'Skip' if you'd like to come back to this later, or 'Delete' to remove this section of your policy. Start your message with a + to directly append.", ephemeral: true })
				break
			case "borders":
				interaction.editReply({ content: "How will your nation treat its borders? (Border Policy) \nSay 'Skip' if you'd like to come back to this later, or 'Delete' to remove this section of your policy. Start your message with a + to directly append.", ephemeral: true })
				break
			case "flag":
				interaction.editReply({ content: "What banner does your nation fly? (Flag) Send an image. \nSay 'Skip' if you'd like to come back to this later, or 'Delete' to remove this section of your policy.", ephemeral: true })
				break
			case "emblem":
				interaction.editReply({ content: "What emblem does your nation display? (Emblem) Send an image, preferably square/round. This image will be preferred as your /say icon. \nSay 'Skip' if you'd like to come back to this later, or 'Delete' to remove this section of your policy.", ephemeral: true })
				break
			case "color":
				interaction.editReply({ content: "What color best accents your nation? (Color) Send a hex code (i.e. #c080ff) but omit the #. \nSay 'Skip' if you'd like to come back to this later, or 'Delete' to remove this section of your policy.", ephemeral: true })
				break
			case "values":
				interaction.editReply({ content: "What will your nation absolutely never (or always) do? (values) \nSay 'Skip' if you'd like to come back to this later, or 'Delete' to remove this section of your policy. Start your message with a + to directly append.", ephemeral: true })
				break
			case "shorthand":
				interaction.editReply({ content: "What's an abbreviation or other short way to refer to your nation? (Shorthand) \nSay 'Skip' if you'd like to come back to this later, or 'Delete' to remove this section of your policy. <25 characters", ephemeral: true })
				break
		}
		const filter = m => interaction.user.id == m.author.id;
		const collector = interaction.channel.createMessageCollector(filter, { max: 1, time: 3600, errors: ["time"] })
		const yield = new Promise(async (resolve, reject) => {
			setTimeout(timeout, 600000)
			function timeout() {
				collector.stop()
				interaction.editReply("Please re-submit! (Timeout)")
				return resolve()
			}
			collector.on('collect', m => {
				if (interaction.user.id == m.author.id) {
					function complete() {
						m.fetch(true).then(fetched => { if (fetched.deletable) fetched.delete().catch(log("error deleting, policy")) })
						collector.stop()
						return resolve()
					}
					if (m.content.toLowerCase() != "skip" && m.content.toLowerCase() != "delete") {
						if (section != "flag" && section != "emblem") {
							if (section == 'color') {
								const colReg = /[a-zA-Z0-9]{6}/
								var color = colReg.exec(m.content)
								log(color)
								if (color != null) {
									policy[section] = "#" + color[0]
								} else {
									finishMessage = "Not a hex code!"
								}
								complete()
							} else {
								var completeSection = policy[section] + m.content.replace("+", "")
								if (m.content.startsWith("+") && completeSection.length < 4000) {
									policy[section] = policy[section] + m.content.replace("+", "")
								} else if (completeSection.length > 4000) {
									finishMessage = "Your section would be too long! (>4k characters)"
								} else {
									policy[section] = m.content
								}
								complete()
							}
						} else if (m.attachments.size > 0) {
							const filename = m.id + "." + m.attachments.first().name.split(".")[1]
							try {
								download(m.attachments.first().url, `./${section}s/` + filename, complete)
							} catch (error) {
								log("download error!")
							}
							policy[section] = filename
						}
					} else {
						if (m.content.toLowerCase() == "delete") delete policy[section]
						complete()
					}
				}
			})
			collector.on('end', collected => {
				collector.stop()
				if (collected.size < 1) return resolve()
			});
		})
		await yield
	})
	interaction.editReply({ content: finishMessage, ephemeral: true })
	const policyEmbed = new MessageEmbed()
		.setColor('#0099ff')
		.setFooter(interaction.user.username + "'s policy")
	var files = []
	var embeds = [policyEmbed]
	Object.keys(policy).forEach(component => {
		log(component)
		if (policy[component] == "" || typeof policy[component] !== 'string') {
			log("missing value")
			delete policy[component]
			return
		}
		function addSection() {
			if (policy[component].length < 1000) {
				policyEmbed.addFields({
					name: `**${component.charAt(0).toUpperCase() + component.slice(1)}**`,
					value: policy[component]
				})
			} else {
				const embed = new MessageEmbed()
					.setColor('#0099ff')
					.setTitle(`**${component.charAt(0).toUpperCase() + component.slice(1)}**`)
					.setDescription(policy[component])
					.setFooter(interaction.user.username + "'s " + component)
				if (Object.keys(policy).includes("color")) {
					embed.setColor(policy.color)
				}
				embeds.push(embed)
			}
		}
		switch (component) {
			case "name":
				policyEmbed.setTitle(policy.name)
				break
			case "shorthand":
				policyEmbed.setAuthor(policy.shorthand)
				break
			case "color":
				policyEmbed.setColor(policy.color)
				break
			case "emblem":
				if (!fs.existsSync("./emblems/" + policy.emblem)) {
					interaction.followUp({ ephemeral: true, content: "Missing Emblem File, please re-add to your policy! Thank you." })
					log("Missing Emblem File")
					break
				}
				const fileEm = new MessageAttachment("./emblems/" + policy.emblem)
				log(fileEm)
				files.push(fileEm)
				policyEmbed.setThumbnail("attachment://" + policy.emblem)
				break
			case "flag":
				if (!fs.existsSync("./flags/" + policy.flag)) {
					interaction.followUp({ ephemeral: true, content: "Missing Flag File, please re-add to your policy! Thank you." })
					log("Missing Flag File")
					break
				}
				const fileFl = new MessageAttachment("./flags/" + policy.flag)
				log(fileFl)
				files.push(fileFl)
				policyEmbed.setImage("attachment://" + policy.flag)
				break
			case "message":
				break
			default:
				addSection()
				break
		}
	})
	var totLen = 0
	Object.keys(policy).forEach(section => {
		totLen += section.length
	})
	if (totLen < 5500) {
		const message = { embeds: embeds, "files": files }
		try {
			let policyMessage = await interaction.channel.messages.fetch(policy.message)
			policyMessage.removeAttachments().then(() => {
				policyMessage.edit(message)
			})
		} catch (error) {
			interaction.channel.send(message).then(message => {
				policy["message"] = message.id
			})
		}
	} else {
		interaction.editReply({
			content: "The total character length of all of your sections has exceeded 5500. Please reduce it below this amount!",
			ephemeral: true
		})
	}
}

function getEmoji(user) {
	return new Promise(resolve => {
		if (!Object.keys(global.data).includes("emoji")) {
			global.data["emoji"] = {}
		}
		if (!Object.keys(global.data.emoji).includes(user.id)) {
			client.guilds.fetch("807665202328764486").then(guild => {
				let avatar = user.avatarURL({ size: 128, dynamic: true })
				
				guild.emojis.create(avatar, user.id).then(emoji => {
					global.data.emoji[user.id] = emoji.toString()
					if (!cleaningEmoji) {
						cleanEmoji(guild)
					}
					resolve(global.data.emoji[user.id])
				}).catch(
					resolve("X")
				)
				// if (avatar.length < 100) {
				// } else {
				// 	resolve("
				// }
			})
		} else {
			resolve(global.data.emoji[user.id])
		}
	})
}
let cleaningEmoji = false
function cleanEmoji(guild) {
	cleaningEmoji = true
	guild.emojis.fetch(null, { force: true }).then(emojis => {
		let usedEmojis = Object.values(global.data.emoji)
		let count = 0
		emojis.forEach(emojiAll => {
			if (!usedEmojis.includes(emojiAll.toString())) emojiAll.delete().then(() => {
				count += 1
				if (count >= emojis.size) cleaningEmoji = false
			})
		})
	})
}

async function say(guild_id, user, interaction) {
	var game = findGameByChannel(guild_id, interaction.channel.id)
	if (!game) {
		interaction.reply({ content: 'Not in a game channel!', ephemeral: true });
		return
	}
	var policy = game.players[user.id].bio
	var response = "Proclaiming!"
	const content = interaction.options.getString('message')
	if (Object.keys(policy).includes("name") && (Object.keys(policy).includes("flag") || Object.keys(policy).includes("emblem"))) {
		global.say.say.sayMessage(game, policy, content, interaction.channel, user)
	} else { response = "Incomplete Policy!" }
	interaction.reply({ content: response, ephemeral: true });
}

global.client.on("messageCreate", message => {
	var game = findGameByChannel(message.guild.id, message.channel.id)
	if (!game) {
		return
	} else if (!Object.keys(game.players).includes(message.author.id)) {
		return
	}
	var policy = game.players[message.author.id].bio
	if (Object.keys(policy).includes("name") && (Object.keys(policy).includes("flag") || Object.keys(policy).includes("emblem"))) {
		if (Object.keys(policy).includes("sayToggle")) {
			if (policy.sayToggle.includes(message.channel.id)) {
				const content = message.content
				const channel = message.channel
				var reply = false
				var files = false
				new Promise(async resolve => {
					if (message.attachments.size > 0) {
						var count = 0
						message.attachments.forEach(attachment => {
							count += 1
							const filename = String(count) + message.id + "." + message.attachments.first().name.split(".")[1]
							download(attachment.url, `./images/` + filename, () => {
								var att = new MessageAttachment('./images/' + filename)
								if (files) { files.push(att) } else { files = [att] }
								complete()
							})
						})
						function complete() {
							if (count == message.attachments.size) {
								return resolve()
							}
						}
					} else return resolve()
				}).then(() => {
					message.fetch(true).then(fetched => { if (fetched.deletable) fetched.delete().catch(log("error deleting, saytoggle")) })
					global.say.say.sayMessage(game, policy, content, channel, message.author, reply, files)
				})
			}
		}
	}
})

global.client.on("messageCreate", message => {
	var game = findGameByChannel(message.guild.id, message.channel.id)
	if (!game) {
		return
	} else if (!Object.keys(game.players).includes(message.author.id)) {
		return
	}
	if (message.author.bot) return
	checkMention(game, message)
})

function checkMention(game, message) {
	Object.keys(game.players).forEach(playerID => {
		const player = game.players[playerID]
		const bio = player.bio
		if (Object.keys(bio).length > 0) {
			if (Object.keys(bio).includes("shorthand")) {
				if (message.content.toLowerCase().includes("!" + bio.shorthand.toLowerCase())) {
					log("shorthand mentioned")
					const Comp = new MessageActionRow()
						.addComponents(
							new MessageButton()
								.setURL(message.url)
								.setLabel('Go To!')
								.setStyle('LINK'),
						);
					client.users.fetch(playerID).then(user => {
						user.send({ content: `You Were Mentioned in Game ${game.id}`, components: [Comp] })
					})
				}
			} else {
				var name = bio.name
				try {
					if (name.length > 25) {
						var words = name.split(" ")
						name = ""
						words.forEach(word => {
							var char = word.charAt(0)
							name += char
						})
					}
				} catch { console.log(bio) }
				if (false) {
					log("shorthand mentioned")
					const Comp = new MessageActionRow()
						.addComponents(
							new MessageButton()
								.setURL(message.url)
								.setLabel('Go To!')
								.setStyle('LINK'),
						);
					client.users.fetch(playerID).then(user => {
						user.send({ content: `You Were Mentioned in Game ${game.id}`, components: [Comp] })
					})
				}
			}
		} else log(playerID)
	})
}



function sayToggle(guild_id, user, interaction) {
	var game = findGameByChannel(guild_id, interaction.channel.id)
	if (!game) {
		interaction.reply({ content: 'Not in a game channel!', ephemeral: true });
		return
	}
	var policy = game.players[user.id].bio
	var response = "Proclaiming!"
	if (Object.keys(policy).includes("name") && (Object.keys(policy).includes("flag") || Object.keys(policy).includes("emblem"))) {
		if (!Object.keys(policy).includes("sayToggle")) {
			policy["sayToggle"] = []
			response = "Toggled On"
			policy.sayToggle.push(interaction.channel.id)
		} else {
			if (policy.sayToggle.includes(interaction.channel.id)) {
				response = "Toggled Off"
				policy.sayToggle.splice(policy.sayToggle.indexOf(interaction.channel.id), 1)
			} else {
				response = "Toggled On"
				policy.sayToggle.push(interaction.channel.id)
			}
		}
	} else { response = "Incomplete Policy!" }
	interaction.reply({ content: response, ephemeral: true });
}

const buttonParse = /((?<={)[0-9]+(?=})).+((?<=\[).+(?=\]))/

client.on('interactionCreate', interaction => {
	if (!running) return
	if (!interaction.isButton()) return;
	var data = buttonParse.exec(interaction.customId)
	log(data, 4)
	log(String(interaction.user.id) + " Pressed Button    " + String(data[2]) + " - " + String(data[1]), 2)
	data[2] = data[2].split(",")
	switch (data[2][0]) {
		case "join":
			joinGame(interaction.guild.id, data[1], interaction.user, interaction)
			break
		case "watch":
			watchGame(interaction.guild.id, data[1], interaction.user, interaction)
			break
		case "policy":
			setPolicyCont(interaction.guild.id, data[1], data[2].slice(1), interaction)
			break
		case "pass":
			break
	}
});
client.on('interactionCreate', interaction => {
	if (!running) return
	if (!interaction.isSelectMenu()) return;
	log(String(interaction.user.id) + " Used Select Menu", 2)
	switch (interaction.customId) {
		case "policy":
			setPolicyCont(interaction.guild.id, findGameByChannel(interaction.guildId, interaction.channelId).id, interaction.values, interaction)
			break
	}
});

// client.on('interactionCreate', interaction => {
// 	if (!interaction.isContextMenu()) return
// 	switch(interaction.CommandName) {
// 		case "goto":
// 			var game = findGameByChannel(interaction.guild.id, interaction.channel.id)
// 			if (!game) {
// 				interaction.reply({ content: 'Not in a game channel!', ephemeral: true });
// 				return
// 			}
// 			const message = interaction.options.getMessage('message')
// 			var res = global.say.getUser(message.id)
// 			if (!res) {
// 				if (Object.keys(game.players).includes(message.author.id)) res = message.author.id
// 			}
// 			if (res) {
// 				var player = game.players[res]
// 				var url = `https://discord.com/channels/${interaction.guild.id}/${game.channels.policies}/${player.bio.message}`
// 				const linkRow = new MessageActionRow()
// 					.addComponents(
// 						new MessageButton()
// 							.setURL(url)
// 							.setLabel(`Go to Policy`)
// 							.setStyle('LINK'),
// 					)
// 				interaction.reply({components: [linkRow], ephemeral: true})
// 			}
// 			break
// 	}
// })

async function asyncForEach(array, callback) {
	for (let index = 0; index < array.length; index++) {
		await callback(array[index], index, array);
	}
}

function findGameByChannel(guild_id, channel_id) {
	const games = global.data[guild_id].games
	var foundGame = false
	games.forEach(game => {
		Object.values(game.channels).forEach(channel => {
			if (channel == channel_id) {
				//log("found game " + game.id)
				foundGame = game
			}
		})
	})
	return foundGame
}

var https = require('https');
var fs = require('fs');

var download = function (url, dest, cb) {
	var file = fs.createWriteStream(dest);
	var request = https.get(url, function (response) {
		response.pipe(file);
		file.on('finish', function () {
			file.close(cb);  // close() is async, call cb after close completes.
		});
	}).on('error', function (err) { // Handle errors
		fs.unlink(dest); // Delete the file async. (But we don't check the result)
		if (cb) cb(err.message);
	});
};

var download = function (url, dest, cb) {
	var file = fs.createWriteStream(dest);
	var request = https.get(url, function (response) {
		response.pipe(file);
		file.on('finish', function () {
			file.close(cb);  // close() is async, call cb after close completes.
		});
	}).on('error', function (err) { // Handle errors
		fs.unlink(dest); // Delete the file async. (But we don't check the result)
		if (cb) cb(err.message);
	});
};
