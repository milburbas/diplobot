const Game = class {
	constructor(name, id) {
		this.state = "pending"
		this.id = id
		this.name = name
		this.data = {
			"turn": 0,
			"reminder": 3600*24,
			"begin": 0,
			"history": []
		}
		this.players = []
	}
}