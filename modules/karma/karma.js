const loglevel = 5
const log_prefix = "\x1b[31mKarma >> \x1b[0m%s"

var karma = require("./karma.json")

const fs = require('fs');

var running = false

function log(message, level = 0) {
	if (level > loglevel) return
	console.log(log_prefix,message)
}

exports.initFn =  {
	init,
	uninit
}

exports.karma =  {
	report,
}

const {MessageEmbed} = require('discord.js');

async function report(guildID, user, interaction) {
	const embed = new MessageEmbed()
		.setColor('#0099ff')
		.setTitle('**Karma Report**')
	var build = new Promise(async resolve => {
		Object.keys(karma[guildID]).forEach(member => {
			interaction.guild.members.fetch(member).then(mem => {
				var name = mem.user.username
				var total = 0
				Object.keys(karma[guildID][member]).forEach(message => {
					log(karma[guildID][member][message])
					total += karma[guildID][member][message].length
				})
				embed.addFields({ name: '**' + name + '**', value: `${String(total)} Karma`, inline: true})
				complete()
			})
		})
		var count = 0
		function complete() {
			count += 1
			log(count)
			if (count == Object.keys(karma[guildID]).length) return resolve()
		}
	})
	await build
	interaction.reply({embeds: [embed], ephemeral: true})
}

function init() {
	running = true
	log("Initialized")
}

function uninit() {
	delete require.cache[require.resolve("./karma.js")];
	running = false
	log("Uninitialized")
}

setInterval(save, 5000)

function save() {
	if (!running) return
	fs.readFile("./modules/karma/karma.json", 'utf8', function(err, data){
		if (data != JSON.stringify(karma)) {
			log("saving database")
			fs.writeFileSync("./modules/karma/karma.json", JSON.stringify(karma))
		}
	});
}
//{"629118165783150605": {"370281623394189322": {"370281623394189322": ["515593074282332183"]}}}
global.client.on("messageReactionAdd", (reaction, user) => {
	reaction.fetch().then(reactionFetched => {
		if (reactionFetched.emoji.name != '❤️') return
		var guildID = reactionFetched.message.guild.id
		var authorID = reactionFetched.message.author.id
		var messageID = reactionFetched.message.id
		if (reactionFetched.message.author.bot) {
			var trueAuthor = global.say.say.getUser(messageID)
			log(trueAuthor)
			if (trueAuthor) {authorID = trueAuthor} else {return}
		}
		if (authorID == user.id) return
		if (!Object.keys(karma).includes(guildID)) {
			karma[guildID] = {}
		}
		if (!Object.keys(karma[guildID]).includes(authorID)) {
			karma[guildID][authorID] = {}
		}
		if (!Object.keys(karma[guildID][authorID]).includes(messageID)) {
			karma[guildID][authorID][messageID] = [user.id]
		} else {
			if (!karma[guildID][authorID][messageID].includes(user.id)) karma[guildID][authorID][messageID].push(user.id)
		}
	})
})

global.client.on("messageReactionRemove", (reaction, user) => {
	reaction.fetch().then(reactionFetched => {
		if (reactionFetched.emoji.name != '❤️') return
		var guildID = reactionFetched.message.guild.id
		var authorID = reactionFetched.message.author.id
		var messageID = reactionFetched.message.id
		if (authorID == user.id) return
		try {
			var messageKarma = karma[guildID][authorID][messageID]
			log([messageKarma, user.id])
			log(messageKarma.indexOf(user.id))
			messageKarma.splice(messageKarma.indexOf(user.id), 1)
		} catch (error) {
			log(error)
		}
	})
})
