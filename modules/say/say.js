
const loglevel = 5
const log_prefix = "\x1b[37mSay >> \x1b[0m%s"

var saylog = require("./saylog.json")

const fs = require('fs');

var running = false

function log(message, level = 0) {
	if (level > loglevel) return
	console.log(log_prefix,message)
}

exports.initFn =  {
	init,
	uninit
}

exports.say =  {
	sayMessage,
	getUser,
}

function init() {
	running = true
	log("Initialized")
}

function uninit() {
	delete require.cache[require.resolve("./karma.js")];
	running = false
	log("Uninitialized")
}

setInterval(save, 5000)

function save() {
	if (!running) return
	fs.readFile("./modules/say/saylog.json", 'utf8', function(err, data){
		if (data != JSON.stringify(saylog)) {
			log("saving database")
			fs.writeFileSync("./modules/say/saylog.json", JSON.stringify(saylog))
		}
	});
}


function getUser(messageID) {
	if (Object.keys(saylog).includes(messageID)) {
		let userID = saylog[messageID]
		return userID
	} else {
		return false
	}
}
async function sayMessage(game, policy, content, channel, user, reply = false, files = false) {
	const polMsg = await global.client.channels.cache.get(game.channels.policies).messages.fetch(policy.message)
	var avatar
	try {
		if (Object.keys(policy).includes("emblem")) {
			avatar = polMsg.embeds[0].thumbnail.url
		} else {
			avatar = polMsg.embeds[0].image.url
		}
	} catch (error) {log(error)}
	const webhooks = await channel.fetchWebhooks()
	const webhook = webhooks.first()
	var name = policy.name
	if (name.length > 25) {
	  if (Object.keys(policy).includes("shorthand")) {
		  name = policy.shorthand
		} else {
			var words = name.split(" ")
			name = ""
			words.forEach(word => {
				var char = word.charAt(0)
				name += char
	  		})
	  	}
	}
	var options = {
	  username: name,
	  avatarURL: avatar
	}
	if (files) options['files'] = files
	if (content) options['content'] = content
	if (!files && (!content || content == '')) return
	if (webhook != undefined) {
		webhook.send(options).then(message => {
            saylog[message.id] = user.id
			global.games.game.checkMention(game, message)
		});
	} else {
		channel.createWebhook('Proclamations', {
			avatar: 'https://i.imgur.com/wyMm1hJ.png',
		}).then( webhook => {
			webhook.send(options).then(message => {
                saylog[message.id] = user.id
            })
		})
	}
}

// client.on('interactionCreate', interaction => {
// 	if (!interaction.isContextMenu()) return
// 	switch(interaction.CommandName) {
// 		case "topolicy":
// 			log("topolicy")
// 			break
// 	}
// })
