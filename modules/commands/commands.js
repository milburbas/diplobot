const commands = require('./commands.json')
const clone = require('clone')

var running = false

const log_prefix = "\x1b[33mComm >> \x1b[0m%s"

const loglevel = 3

function log(message, level = 0) {
	if (level > loglevel) return
	console.log(log_prefix,message)
}

exports.initFn =  {
	init,
	uninit
}

exports.commands = {
	updateCommand
}
function init() {
	Object.keys(global.data).forEach(async (guild_id) => {
		if (guild_id.length > 10) {
			var json = []
			Object.keys(commands).forEach( command => {
				json.push(buildCommand(command, guild_id))
				if (Object.keys(commands[command]).includes("global")) if (commands[command].global) {
				global.client.application.commands.create(buildCommand(command))
				}
			})
			const guild = await global.client.guilds.cache.get(guild_id)
			guild.commands.set(json)
		}
	})
	running = true
	log("Initialized")
}

function uninit() {
	delete require.cache[require.resolve("./commands.js")];
	running = false
	log("Uninitialized")
}

function buildCommand(name, guild_id = false) {
	log("building command:    " + String(name) + " - " + String(guild_id), 3)
	const command = clone(commands[name])
	var command_data = command.data
	if (guild_id) {
	const guild_data = global.data[guild_id]
	command.dynamic_choices.forEach(dynamic_choice => {
		log("found dynamic choice", 4)
		var choices = command_data.options[dynamic_choice.option].choices
		switch(dynamic_choice.type) {
			case "games":
				log("dynamic choice type is games", 4)
				guild_data.games.forEach(game => {
					var choice = {
						"name": game.name,
						"value": String(game.id)
					}
					log(choice, 4)
					choices.push(choice)
				})
				break
			case "pending_games":
				log("dynamic choice type is pending games", 4)
				guild_data.games.forEach(game => {
					if (game.state == "pending") { 
						var choice = {
							"name": game.name,
							"value": String(game.id)
						}
						log(choice, 4)
						choices.push(choice)
					}
				})
				break
			case "ongoing_games":
				log("dynamic choice type is ongoing games", 4)
				guild_data.games.forEach(game => {
					if (game.state == "ongoing") { 
						var choice = {
							"name": game.name,
							"value": String(game.id)
						}
						log(choice, 4)
						choices.push(choice)
					}
				})
				break
		}
	});
	}
	return command_data
}

function updateCommand(name, guild_id) {
	const guild = global.client.guilds.cache.get(guild_id)
	guild.commands.create(buildCommand(name, guild_id))
}

global.client.on('interactionCreate', interaction => {
	if (!running) return
	if (!interaction.isCommand()) return;
	switch(interaction.commandName){
		case "new":
			if (!Object.values(interaction.member._roles).includes(global.data[interaction.guild.id].organizer)) {
				log(String(interaction.member.user.id) + " tried to run /new, lacked organizer role.")
				interaction.reply({ content: 'You aren\'t an organizer!', ephemeral: true });
				return
			}
			log(String(interaction.member.user.id) + " ran /new")
			interaction.reply({ content: 'Creating new game!', ephemeral: true });
			global.games.game.newGame(interaction.guildId)
			break
		case "start":
			if (!Object.values(interaction.member._roles).includes(global.data[interaction.guild.id].organizer)) {
				log(String(interaction.member.user.id) + " tried to run /start, lacked organizer role.")
				interaction.reply({ content: 'You aren\'t an organizer!', ephemeral: true });
				return
			}
			log(String(interaction.member.user.id) + " ran /start")
			interaction.reply({ content: 'Starting game!', ephemeral: true });
			global.games.game.startGame(interaction.guildId, interaction.options._hoistedOptions[0].value)
			break
		case "join":
			if (!Object.values(interaction.member._roles).includes(global.data[interaction.guild.id].organizer)) {
				log(String(interaction.member.user.id) + " tried to run /start, lacked organizer role.")
				interaction.reply({ content: 'You aren\'t an organizer!', ephemeral: true });
				return
			}
			log(String(interaction.member.user.id) + " ran /join")
			global.games.game.joinGame(interaction.guildId, interaction.options.getString('game'), interaction.options.getUser('user'), interaction)
			break
		case "policy":
			log(String(interaction.member.user.id) + " tried to run /policy")
			global.games.game.setPolicy(interaction.guild.id, interaction.user, interaction)
			break
		case "info":
			log(String(interaction.member.user.id) + " tried to run /info")
			global.games.game.info(interaction.guild.id, interaction.user, interaction)
			break
		case "say":
			log(String(interaction.member.user.id) + " tried to run /say")
			global.games.game.say(interaction.guild.id, interaction.user, interaction)
			break
		case "karma":
			log(String(interaction.member.user.id) + " tried to run /karma")
			global.karma.karma.report(interaction.guild.id, interaction.user, interaction)
			break
		case "saytoggle":
			log(String(interaction.member.user.id) + " tried to run /saytoggle")
			global.games.game.sayToggle(interaction.guild.id, interaction.user, interaction)
			break
		case "poll":
			// if (!Object.values(interaction.member._roles).includes(global.data[interaction.guild.id].organizer)) {
			// 	log(String(interaction.member.user.id) + " tried to run /poll, lacked organizer role.")
			// 	interaction.reply({ content: 'You aren\'t an organizer!', ephemeral: true });
			// 	return
			// }
			log(String(interaction.user.id) + " tried to run /poll")
			global.games.game.commandPoll(interaction)
			break
		case "flag":
			log(String(interaction.user.id) + " tried to run /flag")
			global.flagdesigner.flag.design(interaction)
			break
	}
});
