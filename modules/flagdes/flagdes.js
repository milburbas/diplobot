const loglevel = 5
const log_prefix = "\x1b[39mFlag >> \x1b[0m%s"

var running = false

function log(message, level = 0) {
	if (level > loglevel) return
	console.log(log_prefix,message)
}

exports.initFn =  {
	init,
	uninit
}

function init() {
	running = true
	log("Initialized")
}

function uninit() {
	delete require.cache[require.resolve("./flagdesigner.js")];
	running = false
	log("Uninitialized")
}

const { createCanvas, Image } = require('canvas')
// const canvas = createCanvas(200, 200)
// const ctx = canvas.getContext('2d')

const fs = require('fs')


exports.flag = {
	design
}

const { MessageActionRow, MessageButton, MessageAttachment} = require('discord.js');


const buttonParse = /((?<={)[0-9]+(?=})).+((?<=\[).+(?=\]))/


async function design(interaction) {
    async function yield(section) {
        return new Promise(resolve => {
            var active = true
            const handle = interactionButton => {
                if (!active) return
                if (!interactionButton.isButton()) return
                if (interaction.user.id != interactionButton.user.id) return
                var data = buttonParse.exec(interactionButton.customId)
                log(data, 4)
                log(String(interactionButton.user.id) + " Pressed Button    " + String(data[2]) + " - " + String(data[1]), 2)
                data[2] = data[2].split(",")
                switch(data[2][0]) {
                    case "flag":
                        if (data[2][1] == section) {
                            interactionButton.reply({"ephemeral": true}).catch(err => log("sent response", 6))
                            active = false
                            global.client.removeListener("interactionCreate", handle)
                            resolve(data[2])
                            break
                        }
                }
            }
            global.client.on('interactionCreate', handle)
        })
    }
    const aspectrow = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setCustomId("{00}" + "[flag,shape,square]")
					.setLabel('1:1')
					.setStyle('PRIMARY'),
                new MessageButton()
					.setCustomId("{00}" + "[flag,shape,rect-s]")
					.setLabel('3:4')
					.setStyle('PRIMARY'),
                new MessageButton()
					.setCustomId("{00}" + "[flag,shape,rect-n]")
					.setLabel('2:3')
					.setStyle('PRIMARY'),
                new MessageButton()
					.setCustomId("{00}" + "[flag,shape,rect-l]")
					.setLabel('1:2')
					.setStyle('PRIMARY'),
			);
    interaction.reply({content: "Choose your Aspect Ratio (height:length). If you're making an emblem, 1:1 is the best option. The standard for flags is 2:3", components: [aspectrow], ephemeral: true})
    var aspect = await yield("shape")
    aspect = aspect[2]
    const orientrow = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setCustomId("{00}" + "[flag,orientation,horizontal]")
					.setLabel('Horizontal')
					.setStyle('PRIMARY'),
                new MessageButton()
					.setCustomId("{00}" + "[flag,orientation,vertical]")
					.setLabel('Vertical')
					.setStyle('PRIMARY'),
			);
    interaction.editReply({content: "Choose the orientation of the bars.", components: [orientrow], ephemeral: true})
    var orientation = await yield("orientation")
    orientation = orientation[2]
    const colorrow = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setCustomId("{00}" + "[flag,colors,done]")
					.setLabel('Done')
					.setStyle('SUCCESS'),
			);
    interaction.editReply({content: "Send the colors of the bars. They will be divided evenly, from top/left to bottom/right in the order they are received, across the flag.\nHex codes, omitting the # (ex. c080ff)", components: [colorrow], ephemeral: true})
    var colors = []
    function MakeCollector() {
        const filter =  m => interaction.user.id == m.author.id;
        const collector = interaction.channel.createMessageCollector(filter, {max: 1, time: 3600, errors: ["time"]})
        setTimeout(timeout, 600000)
        function timeout() {
            collector.stop()
            interaction.editReply("Please re-submit! (Timeout)")
        }
        return collector
    }
    var colorCollector = MakeCollector()
    colorCollector.on('collect', m => {
        if (interaction.user.id == m.author.id){
            const colReg = /[a-zA-Z0-9]{6}/
            var color = colReg.exec(m.content)
            log(color)
            if (color != null) {
                colors.push("#" + color[0])
            } else {
            }
            m.fetch(true).then(fetched => { if (fetched.deletable) fetched.delete().catch(log("error deleting, flag")) })
        }
    })
    await yield("colors")
    colorCollector.stop()
    var flag
    switch(orientation) {
        case "horizontal":
            flag = barsH(colors, aspect)
            break
        case "vertical":
            flag = barsV(colors, aspect)
            break
    }
    const elemrow = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setCustomId("{00}" + "[flag,elem,done]")
					.setLabel('Done')
					.setStyle('SUCCESS'),
				new MessageButton()
					.setCustomId("{00}" + "[flag,elem,emblem]")
					.setLabel('Emblem')
					.setStyle('PRIMARY'),
				new MessageButton()
					.setCustomId("{00}" + "[flag,elem,bar]")
					.setLabel('Bar')
					.setStyle('PRIMARY'),
            );
    var ElementsProcess = new Promise(resolve => {
        async function addElem() {
        interaction.followUp({content: "Add an emblem, or custom bar.", components: [elemrow], ephemeral: true})
        var newElem = await yield("elem")
        newElem = newElem[2]
        switch(newElem) {
            case "emblem":
                interaction.followUp({content: "Add an emblem. Send an image, with numbers in the format of `scale, distance from left edge, distance from top` All numbers /100 as a percent of the total image. Best results if the background is transparent.", components: [], ephemeral: true})
                var emblemCollector = MakeCollector()
                emblemCollector.on('collect', async m => {
                    if (interaction.user.id == m.author.id){
                        if (m.attachments.size > 0) {
                            const dataReg = /([0-1]?[0-9]{1,2})(?:,\s*)([0-1]?[0-9]{1,2})(?:,\s*)([0-1]?[0-9]{1,2})/
                            if (dataReg.exec(m.content) != null) {
                                const data = dataReg.exec(m.content).slice(1,4)
                                const filename = m.id + "." + m.attachments.first().name.split(".")[1]
                                await new Promise(resolve => {
                                    download(m.attachments.first().url, "./images/" + filename, resolve)
                                })
                                var imageData = {
                                    "image": "./images/" + filename,
                                    "startX": parseInt(data[1]),
                                    "startY": parseInt(data[2]),
                                    "scale": parseInt(data[0])
                                }
                                flag = await addImage(flag.canvas, imageData)
                                log(imageData)
                                interaction.followUp({files: [new MessageAttachment(flag.buffer)], ephemeral: true})
                            } else {
                                interaction.followUp({content: "Not valid!", ephemeral: true})
                            }
                        }
                        m.fetch(true).then(fetched => { if (fetched.deletable) fetched.delete().catch(log("error deleting, flag")) })
                        emblemCollector.stop()
                        addElem()
                    }
                })
                break
            case "bar":
                interaction.followUp({content: "Add a bar. Send data in the format of `color, distance of top-left corner to left edge, distance of top-left corner to top, width, height` All numbers /100 as a percent of the total image.", ephemeral: true})
                var barCollector = MakeCollector()
                log("bar started")
                barCollector.on('collect', async m => {
                    if (interaction.user.id == m.author.id){
                        const dataReg = /([a-zA-Z0-9]{6})(?:,\s*)([0-1]?[0-9]{1,2})(?:,\s*)([0-1]?[0-9]{1,2})(?:,\s*)([0-1]?[0-9]{1,2})(?:,\s*)([0-1]?[0-9]{1,2})/
                        if (dataReg.exec(m.content) != null) {
                            const data = dataReg.exec(m.content).slice(1,6)
                            var barData = {
                                "color": "#" + data[0],
                                "startX": parseInt(data[1]),
                                "startY": parseInt(data[2]),
                                "width": parseInt(data[3]),
                                "height": parseInt(data[4])
                                }
                            flag = addBar(flag.canvas, barData)
                            log("bar added")
                            interaction.followUp({files: [new MessageAttachment(flag.buffer)], ephemeral: true})
                        } else {
                            interaction.followUp({content: "Not valid!", ephemeral: true})
                        }
                        log("bar done")
                        m.fetch(true).then(fetched => { if (fetched.deletable) fetched.delete().catch(log("error deleting, flag")) })
                        barCollector.stop()
                        addElem()
                    }
                })
                break
            case "done":
                resolve()
                break
        }
        }
        addElem()
    })
    await ElementsProcess
    interaction.user.send({files: [new MessageAttachment(flag.buffer)]})
    interaction.followUp({files: [new MessageAttachment(flag.buffer)], ephemeral: true})
}

var https = require('https');

var download = function(url, dest, cb) {
  var file = fs.createWriteStream(dest);
  var request = https.get(url, function(response) {
    response.pipe(file);
    file.on('finish', function() {
      file.close(cb);  // close() is async, call cb after close completes.
    });
  }).on('error', function(err) { // Handle errors
    fs.unlink(dest); // Delete the file async. (But we don't check the result)
    if (cb) cb(err.message);
  });
};

function barsH(colors, aspect) {
    var dim = [1000,1000]
    switch(aspect) {
        case "square":
            dim = [1000,1000]
            break
        case "rect-s":
            dim = [1333,1000]
            break
        case "rect-n":
            dim = [1500,1000]
            break
        case "rect-l":
            dim = [2000,1000]
            break
    }
    const canvas = createCanvas(dim[0], dim[1])
    const ctx = canvas.getContext('2d')
    var index = 0
    colors.forEach(color => {
        index += 1
        ctx.fillStyle = color
        ctx.strokeStyle = color
        var vOffset = (dim[1] / colors.length) * (index - 1)
        ctx.fillRect(0, 0 + vOffset - 1, dim[0], dim[1] / colors.length + 1)
    })
    return {"buffer": canvas.toBuffer(), "canvas": canvas}
}

function barsV(colors, aspect) {
    var dim = [1000,1000]
    switch(aspect) {
        case "square":
            dim = [1000,1000]
            break
        case "rect-s":
            dim = [1333,1000]
            break
        case "rect-n":
            dim = [1500,1000]
            break
        case "rect-l":
            dim = [2000,1000]
            break
    }
    const canvas = createCanvas(dim[0], dim[1])
    const ctx = canvas.getContext('2d')
    var index = 0
    colors.forEach(color => {
        index += 1
        ctx.fillStyle = color
        ctx.strokeStyle = color
        var hOffset = (dim[0] / colors.length) * (index - 1)
        ctx.fillRect(0 + hOffset, 0, dim[0] / colors.length, dim[1])
    })
    return {"buffer": canvas.toBuffer(), "canvas": canvas}
}

// bardata {"color": "#e9e0f9", "startX": 45, "startY": 0, "width": 10, "height": 100} white vertical bar transversing central 10% of image

function addBar(canvas, barData) {
    const ctx = canvas.getContext('2d')
    ctx.fillStyle = barData.color
    ctx.strokeStyle = barData.color
    const widInc = canvas.width / 100
    const heiInc = canvas.height / 100
    ctx.fillRect(widInc * barData.startX, heiInc * barData.startY, widInc * barData.width, heiInc * barData.height)
    return {"buffer": canvas.toBuffer(), "canvas": canvas}
}

// imagedata {"image": "./flags/876958381980647474.png", "startX": 45, "startY": 45, "scale": 10} image taking up center 10% of canvas

function addImage(canvas, imageData) {
    return new Promise(function(resolve, reject) {
        const ctx = canvas.getContext('2d')
        const img = new Image()
        const widInc = canvas.width / 100
        const heiInc = canvas.height / 100
        log(widInc * imageData.startX)
        img.onload = () => {
            ctx.drawImage(img, widInc * imageData.startX, heiInc * imageData.startY, heiInc * imageData.scale, heiInc * imageData.scale)
            resolve({"buffer": canvas.toBuffer(), "canvas": canvas})
            }
        img.onerror = err => { reject(err) }
        img.src = imageData.image
    })
}
// var imagedata = {"image": "./flags/876958381980647474.png", "startX": 3.3, "startY": 5, "scale": 30}
// var canvas = barsH(["#e9e0f9", "#ffGGo2", "#e9e0f9", "#ffGGo2", "#e9e0f9", "#ffGGo2"], "RectN").canvas
// var bardata = {"color": "#e9e0f9", "startX": 0, "startY": 0, "width": 26.6, "height": 40}

// async function gen() {
//     var flag = await addImage(addBar(canvas, bardata).canvas, imagedata)
//     fs.writeFile('./images/testTris.png', flag.buffer, (err) => {
//         console.log(err)
//     })
// }
// gen()
